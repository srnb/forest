enablePlugins(ScalaNativePlugin)

lazy val sdl2 = ProjectRef(uri("git://github.com/regb/scalanative-graphics-bindings.git"), "sdl2")

lazy val forest = (project in file(".")).settings(
  name := "forest",
  organization := "com.tsunderebug",
  version := "0.0.1",
  scalaVersion := "2.11.12",
  nativeLinkStubs := true,
  scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")
).dependsOn(sdl2)
