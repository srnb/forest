package com.tsunderebug.forest.window

import sdl2.SDL

import scala.scalanative.native._

sealed trait Event

object Event {

  def poll: Option[Event] = Zone { implicit z =>
    val p = alloc[SDL.SDL_Event]
    SDL.SDL_PollEvent(p) match {
      case 0 => None
      case _ =>
        (!p._1).toLong match {
          case 0x100 => Some(Quit((!p.cast[Ptr[SDL.SDL_QuitEvent]]._2).toLong))
          case 0x200 =>
            val ce = p.cast[Ptr[SDL.SDL_WindowEvent]]
            val wid = !ce._3
            val we = WindowEvent.WindowEventID(!ce._4)
            Some(WindowEvent((!ce._2).toLong, Window(wid), we, !ce._8, !ce._9))
        }
    }
  }

  case class Quit(timestamp: Long) extends Event

  case class WindowEvent(timestamp: Long, window: Window, event: WindowEvent.WindowEventID, data1: Int, data2: Int) extends Event

  object WindowEvent {

    sealed trait WindowEventID {
      def id: UByte
    }

    object WindowEventID {
      def apply(id: UByte): WindowEventID = id.toInt match {
        case 1 => Shown
        case 2 => Hidden
      }
    }

    case object Shown extends WindowEventID {
      override def id: UByte = 1.toUByte
    }

    case object Hidden extends WindowEventID {
      override def id: UByte = 2.toUByte
    }

  }

}
