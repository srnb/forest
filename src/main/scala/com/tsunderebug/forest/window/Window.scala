package com.tsunderebug.forest.window

import sdl2.SDL

import scala.scalanative.native._

object Window {

  sealed trait WindowPos {
    def toInt: Int
  }

  object WindowPos {
    case object Centered extends WindowPos {
      override def toInt: Int = 0x2FFF0000
    }
    case object Undefined extends WindowPos {
      override def toInt: Int = 0x1FFF0000
    }
  }

  sealed trait WindowFlag {
    def toInt: UInt
  }

  object WindowFlag {
    case object Fullscreen extends WindowFlag {
      override def toInt: UInt = 0x00000001.toUInt
    }
  }

  def apply(title: String, x: Either[WindowPos, Int], y: Either[WindowPos, Int], w: Int, h: Int, flags: Set[WindowFlag]): Window = Zone { implicit z =>
    val ix = x.fold((wp: WindowPos) => wp.toInt, (i: Int) => i)
    val iy = y.fold((wp: WindowPos) => wp.toInt, (i: Int) => i)
    val fl = flags.map(_.toInt).reduce((f1, f2) => f1 | f2)
    val wptr = SDL.SDL_CreateWindow(toCString(title), ix, iy, w, h, fl)
    new Window(wptr)
  }

  def apply(id: UInt): Window = new Window(SDL.SDL_GetWindowFromID(id))

}

class Window private(wptr: Ptr[SDL.SDL_Window]) {

  def id: UInt = SDL.SDL_GetWindowID(wptr)
  def destroy(): Unit = SDL.SDL_DestroyWindow(wptr)

}
