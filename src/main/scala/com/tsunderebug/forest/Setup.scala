package com.tsunderebug.forest

import com.tsunderebug.forest.window.Event
import sdl2.SDL

import scala.scalanative.native._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.scalanative.native.UInt

case class Setup(
                  initialize: () => Either[Throwable, Boolean],
                  eventHandler: Event => Unit,
                  loop: () => Boolean,
                  render: () => Unit,
                  cleanup: () => Unit
                ) {

  def start: Future[Either[Throwable, Unit]] = Future {
    SDL.SDL_Init(InitFlag.everything)
    initialize() match {
      case Left(e) => Left(e)
      case Right(false) => Right(Unit)
      case Right(true) =>
        Stream.continually(Event.poll).takeWhile(_.isDefined).map(_.get).foreach(eventHandler)
        while (loop()) {
          render()
        }
        cleanup()
        SDL.SDL_Quit()
        Right(Unit)
    }
  }

  sealed trait InitFlag {
    def toInt: UInt
  }

  object InitFlag {
    val everything: UInt = Timer.toInt | Audio.toInt | Video.toInt | Joystick.toInt | Haptic.toInt | GameController.toInt
    case object Timer extends InitFlag {
      override def toInt: UInt = 0x00000001.toUInt
    }
    case object Audio extends InitFlag {
      override def toInt: UInt = 0x00000010.toUInt
    }
    case object Video extends InitFlag {
      override def toInt: UInt = Events.toInt | 0x00000020.toUInt
    }
    case object Joystick extends InitFlag {
      override def toInt: UInt = Events.toInt | 0x00000200.toUInt
    }
    case object Haptic extends InitFlag {
      override def toInt: UInt = 0x00001000.toUInt
    }
    case object GameController extends InitFlag {
      override def toInt: UInt = Joystick.toInt | 0x00002000.toUInt
    }
    case object Events extends InitFlag {
      override def toInt: UInt = 0x0000400.toUInt
    }
  }

}
